const express = require('express');
const yargs = require('yargs');
const fs = require('fs');
const graphqlHTTP = require('express-graphql');
const { buildSchema } = require('graphql');

const schema = buildSchema(fs.readFileSync('./server/schema.gql', 'utf8'));
const argv = yargs.argv;
const app = express();

const HEROES = [
  { id: 11, name: 'Dr Nice' },
  { id: 12, name: 'Narco' },
  { id: 13, name: 'Bombasto' },
  { id: 14, name: 'Celeritas' },
  { id: 15, name: 'Magneta' },
  { id: 16, name: 'RubberMan' },
  { id: 17, name: 'Dynama' },
  { id: 18, name: 'Dr IQ' },
  { id: 19, name: 'Magma' },
  { id: 20, name: 'Tornado' },
  { id: 21, name: '2 Dr Nice' },
  { id: 22, name: '2 Narco' },
  { id: 23, name: '2 Bombasto' },
  { id: 24, name: '2 Celeritas' },
  { id: 25, name: '2 Magneta' },
  { id: 26, name: '2 RubberMan' },
  { id: 27, name: '2 Dynama' },
  { id: 28, name: '2 Dr IQ' },
  { id: 29, name: '2 Magma' },
  { id: 30, name: '2 Tornado' }
];
let FAKE_ID_COUNTER = 30;

class Hero {
  constructor(id, { name }) {
    this.id = id;
    this.name = name;
  }
}

const root = {
  getHeroes: ({ pageSize = 10, pageNumber = 0 }) => {
    return HEROES.slice(pageNumber * pageSize, pageNumber * pageSize + pageSize);
  },
  createHero: ({ input }) => {
    const hero = new Hero(++FAKE_ID_COUNTER, input);
    HEROES.push(hero);
    return hero;
  }
};

app.use(
  '/graphql',
  graphqlHTTP({
    schema: schema,
    rootValue: root,
    graphiql: true
  })
);

const port = argv.port || 4000;
app.listen(port, () => console.log(`App listening on port ${port}!`));
