function createResponse(body) {
    return {
        statusCode: 200,
        body: JSON.stringify(body, null, 2)
    }
};

module.exports.createResponse = createResponse;