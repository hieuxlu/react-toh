const createResponse = require('./helper/response').createResponse;

const HEROES = [
  { id: 11, name: 'Dr Nice' },
  { id: 12, name: 'Narco' },
  { id: 13, name: 'Bombasto' },
  { id: 14, name: 'Celeritas' },
  { id: 15, name: 'Magneta' },
  { id: 16, name: 'RubberMan' },
  { id: 17, name: 'Dynama' },
  { id: 18, name: 'Dr IQ' },
  { id: 19, name: 'Magma' },
  { id: 20, name: 'Tornado' }
];

module.exports.getUsers = async (event) => {
  return createResponse(HEROES);
};

module.exports.getUserById = async event => {
  const id = +event.pathParameters.id;
  const hero = HEROES.filter(item => item.id === id);
  return createResponse(hero[0]);
};