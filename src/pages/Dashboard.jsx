import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import './Dashboard.css';
import { getHeroes } from '../redux/actions';

class Dashboard extends Component {
  componentDidMount() {
    this.props.dispatch(getHeroes());
  }

  render() {
    const heroItems = this.props.heroes.map(hero => (
      <Link to={`/detail/${hero.id}`} key={hero.id} className="col-1-4">
        <div className="module hero">
          <h4>{hero.name}</h4>
        </div>
      </Link>
    ));
    return (
      <div>
        <h3>Top Heroes</h3>
        <div className="grid grid-pad">{heroItems}</div>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return { heroes: state.heroes.heroes };
};

export default connect(mapStateToProps)(Dashboard);
