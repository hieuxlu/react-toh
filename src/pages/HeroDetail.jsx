import React, { Component } from 'react';
import { connect } from 'react-redux';
import { changeHeroName } from '../redux/actions';
import { withRouter } from 'react-router-dom';

class HeroDetail extends Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.state = {};
  }

  handleChange(event) {
    this.props.dispatch(
      changeHeroName({
        id: this.props.id,
        name: event.target.value
      })
    );
  }

  goBack() {
    this.props.history.goBack();
  }

  save() {}

  render() {
    return (
      <div>
        {this.props.id && (
          <div>
            <h2>{this.props.name} Details</h2>
            <div>
              <span>id: </span>
              {this.props.id}
            </div>
            <div>
              <label>
                name:
                <input value={this.props.name} placeholder="name" onChange={this.handleChange} />
              </label>
            </div>
            <button onClick={() => this.goBack()}>go back</button>
            <button onClick={() => this.save()}>save</button>
          </div>
        )}
      </div>
    );
  }
}

export default withRouter(connect()(HeroDetail));
