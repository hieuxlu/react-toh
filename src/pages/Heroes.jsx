import React, { Component } from 'react';
import { connect } from 'react-redux';

import './Heroes.css';
import HeroDetail from './HeroDetail';
import { selectHero, getHeroes, addHero } from '../redux/actions';

class Heroes extends Component {
  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
    this.state = {
      heroName: ''
    };
  }

  componentDidMount() {
    this.props.dispatch(getHeroes());
  }

  handleClick(event, hero) {
    this.props.dispatch(selectHero(hero));
  }

  delete() {}

  add() {
    this.props.dispatch(addHero({ name: this.state.heroName }));
    this.setState({
      heroName: ''
    });
  }

  handleChange(value) {
    this.setState({
      heroName: value
    });
  }

  render() {
    const heroItems = this.props.heroes.map(hero => (
      <li key={hero.id}>
        <div onClick={e => this.handleClick(e, hero)}>
          <span className="badge">{hero.id}</span> {hero.name}
        </div>
        <button className="delete" title="delete hero" onClick={() => this.delete()}>
          x
        </button>
      </li>
    ));

    return (
      <div>
        <h2>My Heroes</h2>

        <div>
          <label>Hero name: </label>
          <input
            type="text"
            name="heroName"
            value={this.state.heroName}
            placeholder="Hero name"
            onChange={e => this.handleChange(e.target.value)}
          />
          <button onClick={() => this.add()}>add</button>
        </div>

        <ul className="heroes">{heroItems}</ul>

        {this.props.selectedHero && <HeroDetail name={this.props.selectedHero.name} id={this.props.selectedHero.id}></HeroDetail>}
      </div>
    );
  }
}

const mapStateToProps = (state, props) => {
  return { ...state.heroes };
};

export default connect(mapStateToProps)(Heroes);
