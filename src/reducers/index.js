import { combineReducers } from 'redux';
import dashboard from './dashboard';
import heroes from './heroes';

export default combineReducers({ dashboard, heroes });
