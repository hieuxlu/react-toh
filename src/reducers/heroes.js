import { SELECT_HERO, CHANGE_HERO_NAME, GET_HEROES_SUCCESS, ADD_HERO_SUCCESS } from '../redux/actions';

const heroes = (state = { selectedHero: {}, heroes: [] }, action) => {
  switch (action.type) {
    case GET_HEROES_SUCCESS:
      state = {
        ...state,
        heroes: action.payload
      };
      break;
    case SELECT_HERO:
      state = {
        ...state,
        selectedHero: { ...action.payload }
      };
      break;
    case CHANGE_HERO_NAME:
      const heroes = [...state.heroes];
      var heroIdx = heroes.findIndex(hero => hero.id === action.payload.id);

      if (heroIdx < 0) {
        return state;
      }

      var heroItem = {
        ...state.selectedHero,
        name: action.payload.name
      };

      heroes[heroIdx] = heroItem;

      state = {
        ...state,
        selectedHero: {
          ...state.selectedHero,
          name: action.payload.name
        },
        heroes: heroes
      };
      break;

    case ADD_HERO_SUCCESS:
      state = {
        ...state,
        heroes: [...state.heroes]
      };
      state.heroes.push(action.payload);
      break;
    default:
      break;
  }

  return state;
};

export default heroes;
