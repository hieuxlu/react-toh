import axios from 'axios';
import { loader } from 'graphql.macro';

export const GET_HEROES = 'GET_HEROES';
export const GET_HEROES_SUCCESS = 'GET_HEROES_SUCCESS';
export const ADD_HERO_SUCCESS = 'ADD_HERO_SUCCESS';
export const SELECT_HERO = 'SELECT_HERO';
export const CHANGE_HERO_NAME = 'CHANGE_HERO_NAME';

export const getHeroes = () => dispatch =>
  axios
    .post('graphql', {
      query: loader('../graphql/getHeroes.gql').loc.source.body
    })
    .then(res => dispatch(getHeroesSuccess(res.data.data.getHeroes)));

export const getHeroesSuccess = data => ({
  type: GET_HEROES_SUCCESS,
  payload: data
});

export const addHero = input => dispatch =>
  axios
    .post('graphql', {
      query: loader('../graphql/addHero.gql').loc.source.body,
      variables: { input }
    })
    .then(res => dispatch(addHeroSuccess(res.data.data.createHero)));

export const addHeroSuccess = data => ({
  type: ADD_HERO_SUCCESS,
  payload: data
});

export const selectHero = id => ({
  type: SELECT_HERO,
  payload: id
});

export const changeHeroName = data => ({
  type: CHANGE_HERO_NAME,
  payload: data
});
