import React from 'react';
import { Provider } from 'react-redux';

import Dashboard from './pages/Dashboard';
import Heroes from './pages/Heroes';
import HeroDetail from './pages/HeroDetail';
import './App.css';
import { Route, Link } from 'react-router-dom';

function App({ store }) {
  const title = 'Tour of Heroes';

  return (
    <Provider store={store}>
      <h1>{title}</h1>
      <nav>
        <Link to="/dashboard">Dashboard</Link>
        <Link to="/heroes">Heroes</Link>
      </nav>
      <Route exact path="/" component={Dashboard}></Route>
      <Route path="/dashboard" component={Dashboard}></Route>
      <Route path="/heroes" component={Heroes} />
      <Route path="/detail/:id" component={HeroDetail} />
    </Provider>
  );
}

export default App;
